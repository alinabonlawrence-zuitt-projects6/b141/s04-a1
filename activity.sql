a.)
SELECT * FROM artists WHERE name LIKE "%d%";

+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady Gaga     |
|  6 | Ariana Grande |
+----+---------------+

b.) 
SELECT * FROM songs where length < 230;

+----+------------+----------+--------------------------+----------+
| id | title      | length   | genre                    | album_id |
+----+------------+----------+--------------------------+----------+
|  5 | Love Story | 00:02:13 | Country Pop              |        3 |
|  7 | Red        | 00:02:04 | Country                  |        4 |
|  8 | Black Eyes | 00:02:21 | Rock n Roll              |        5 |
|  9 | Shallow    | 00:02:01 | Country, Rock, Folk Rock |        5 |
| 15 | 24K Magic  | 00:02:07 | Funk, Disco, R&B         |       11 |
+----+------------+----------+--------------------------+----------+

c.)
SELECT albums.name AS album_name, songs.title AS song_name, songs.length AS song_length FROM albums
JOIN songs ON albums.id = songs.album_id;

+-----------------+----------------+-------------+
| album_name      | song_name      | song_length |
+-----------------+----------------+-------------+
| Psy 6           | Gangnam Style  | 00:02:53    |
| Trip            | Kundiman       | 00:02:34    |
| Trip            | Kisapmata      | 00:03:19    |
| Fearless        | Fearless       | 00:02:46    |
| Fearless        | Love Story     | 00:02:13    |
| Red             | State of Grace | 00:03:13    |
| Red             | Red            | 00:02:04    |
| A Star Is Born  | Black Eyes     | 00:02:21    |
| A Star Is Born  | Shallow        | 00:02:01    |
| Born This Way   | Born This Way  | 00:02:52    |
| Purpose         | Sorry          | 00:02:32    |
| Believe         | Boyfriend      | 00:02:51    |
| Dangerous Woman | Into You       | 00:02:42    |
| Thank U, Next   | Thank U, Next  | 00:02:36    |
| 24K Magic       | 24K Magic      | 00:02:07    |
| Earth to Mars   | Lost           | 00:02:32    |
+-----------------+----------------+-------------+

d.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id AND albums.name LIKE "%a%";

+----+---------------+----+-----------------+------------+-----------+
| id | name          | id | name            | year       | artist_id |
+----+---------------+----+-----------------+------------+-----------+
|  3 | Taylor Swift  |  3 | Fearless        | 2008-01-01 |         3 |
|  4 | Lady Gaga     |  5 | A Star Is Born  | 2018-01-01 |         4 |
|  4 | Lady Gaga     |  6 | Born This Way   | 2011-01-01 |         4 |
|  6 | Ariana Grande |  9 | Dangerous Woman | 2016-01-01 |         6 |
|  6 | Ariana Grande | 10 | Thank U, Next   | 2019-01-01 |         6 |
|  7 | Bruno Mars    | 11 | 24K Magic       | 2016-01-01 |         7 |
|  7 | Bruno Mars    | 12 | Earth to Mars   | 2011-01-01 |         7 |
+----+---------------+----+-----------------+------------+-----------+

e.)
SELECT * FROM albums ORDER BY name DESC LIMIT 4;

+----+---------------+------------+-----------+
| id | name          | year       | artist_id |
+----+---------------+------------+-----------+
|  2 | Trip          | 1996-01-01 |         1 |
| 10 | Thank U, Next | 2019-01-01 |         6 |
|  4 | Red           | 2012-01-01 |         3 |
|  7 | Purpose       | 2015-01-01 |         5 |
+----+---------------+------------+-----------+

f.)

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.name DESC, songs.title ASC;

+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
| id | name            | year       | artist_id | id | title          | length   | genre                                 | album_id |
+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+
|  2 | Trip            | 1996-01-01 |         1 |  3 | Kisapmata      | 00:03:19 | OPM                                   |        2 |
|  2 | Trip            | 1996-01-01 |         1 |  2 | Kundiman       | 00:02:34 | OPM                                   |        2 |
| 10 | Thank U, Next   | 2019-01-01 |         6 | 14 | Thank U, Next  | 00:02:36 | Pop, R&B                              |       10 |
|  4 | Red             | 2012-01-01 |         3 |  7 | Red            | 00:02:04 | Country                               |        4 |
|  4 | Red             | 2012-01-01 |         3 |  6 | State of Grace | 00:03:13 | Alternative Rock                      |        4 |
|  7 | Purpose         | 2015-01-01 |         5 | 11 | Sorry          | 00:02:32 | Dancehall-poptropical housemoombahton |        7 |
|  1 | Psy 6           | 2012-01-01 |         2 |  1 | Gangnam Style  | 00:02:53 | K-Pop                                 |        1 |
|  3 | Fearless        | 2008-01-01 |         3 |  4 | Fearless       | 00:02:46 | Pop Rock                              |        3 |
|  3 | Fearless        | 2008-01-01 |         3 |  5 | Love Story     | 00:02:13 | Country Pop                           |        3 |
| 12 | Earth to Mars   | 2011-01-01 |         7 | 16 | Lost           | 00:02:32 | Pop                                   |       12 |
|  9 | Dangerous Woman | 2016-01-01 |         6 | 13 | Into You       | 00:02:42 | EDM House                             |        9 |
|  6 | Born This Way   | 2011-01-01 |         4 | 10 | Born This Way  | 00:02:52 | Electropop                            |        6 |
|  8 | Believe         | 2012-01-01 |         5 | 12 | Boyfriend      | 00:02:51 | Pop                                   |        8 |
|  5 | A Star Is Born  | 2018-01-01 |         4 |  8 | Black Eyes     | 00:02:21 | Rock n Roll                           |        5 |
|  5 | A Star Is Born  | 2018-01-01 |         4 |  9 | Shallow        | 00:02:01 | Country, Rock, Folk Rock              |        5 |
| 11 | 24K Magic       | 2016-01-01 |         7 | 15 | 24K Magic      | 00:02:07 | Funk, Disco, R&B                      |       11 |
+----+-----------------+------------+-----------+----+----------------+----------+---------------------------------------+----------+